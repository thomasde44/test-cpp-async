#include <coroutine>
#include <cstdint>
#include <exception>
#include <iostream>
#include <random>
#include <vector>
#include <future>
#include <set>
#include <ranges>
#include <numeric>
#include <algorithm>
#include <random>
using namespace std;

template <typename T>
struct Generator
{
    // The class name 'Generator' is our choice and it is not required for coroutine
    // magic. Compiler recognizes coroutine by the presence of 'co_yield' keyword.
    // You can use name 'MyGenerator' (or any other name) instead as long as you include
    // nested struct promise_type with 'MyGenerator get_return_object()' method.

    struct promise_type;
    using handle_type = std::coroutine_handle<promise_type>;

    struct promise_type // required
    {
        T value_;
        std::exception_ptr exception_;

        Generator get_return_object()
        {
            return Generator(handle_type::from_promise(*this));
        }
        std::suspend_always initial_suspend() { return {}; }
        std::suspend_always final_suspend() noexcept { return {}; }
        void unhandled_exception() { exception_ = std::current_exception(); } // saving
        // exception

        template <std::convertible_to<T> From> // C++20 concept
        std::suspend_always yield_value(From&& from)
        {
            value_ = std::forward<From>(from); // caching the result in promise
            return {};
        }
        void return_void() { }
    };

    handle_type h_;

    Generator(handle_type h)
        : h_(h)
    {
    }
    ~Generator() { h_.destroy(); }
    explicit operator bool()
    {
        fill(); // The only way to reliably find out whether or not we finished coroutine,
        // whether or not there is going to be a next value generated (co_yield)
        // in coroutine via C++ getter (operator () below) is to execute/resume
        // coroutine until the next co_yield point (or let it fall off end).
        // Then we store/cache result in promise to allow getter (operator() below
        // to grab it without executing coroutine).
        return !h_.done();
    }
    T operator()()
    {
        fill();
        full_ = false; // we are going to move out previously cached
        // result to make promise empty again
        return std::move(h_.promise().value_);
    }

private:
    bool full_ = false;

    void fill()
    {
        if (!full_)
        {
            h_();
            if (h_.promise().exception_)
                std::rethrow_exception(h_.promise().exception_);
            // propagate coroutine exception in called context

            full_ = true;
        }
    }
};

class two_n {
private:
    int n;
    int nsquared;
public:
    two_n(int n_, int nsquared_) : n{ n_ }, nsquared{ nsquared_ } {}
    int get_n() const {
        return n;
    }
    int get_nsquared() const {
        return nsquared;
    }
};


Generator<int>
random_sequence()
{
    std::mt19937 mt{};
    for (int i = 0; i < 100; i++) {
        std::uint64_t a = mt() % 60;
        co_yield a;
    }
    co_return;
}

auto my_rand() -> int
{
    static bool const dummy = (srand(time(0)), true);
    return rand();
}


int main()
{
    vector<future<two_n>> tasks;
    vector<int> random_numbers;
    auto ar = random_sequence();
    for (int j = 0; ar; j++) {
        int number = ar();
        auto fut = std::async(std::launch::async, [number] {
            int number2 = number * number;
        two_n val(number, number2);
        return val;
            });

        tasks.push_back(std::move(fut));
    }

    cout << "\n\n\n";
    std::for_each(tasks.begin(), tasks.end(), [&random_numbers](future<two_n>& a) {
        auto b = a.get().get_n();
    random_numbers.push_back(b);
        });

    // store the random numbers generated with the tasks in a multiset
    multiset<int, greater<int>> ms;
    for (int i = 0; i < random_numbers.size(); i++) {
        ms.insert(random_numbers[i]);
    }

    // lambdas for std::views
    auto even = [](int i) { return 0 == i % 2; };
    auto odd = [](int i) { return 0 != i % 2; };
    auto greater = [](int i) { return 0 != i > 30; };


    // use std::views to find even values in the set
    for (int i : ms | std::views::filter(even)) {
        cout << i << "\n";
    }
    // use std::views to find odd values in the set
    for (int i : ms | std::views::filter(odd)) {
        cout << i << "\n";
    }
    // use std::views to find values greater than 30 values in the set
    for (int i : ms | std::views::filter(greater)) {
        cout << i << "\n";
    }
    for (int i : ms | std::views::filter(greater)) {
        cout << i << "\n";
    }
    // sum the multiset with std::accumulate
    int sum = std::accumulate(ms.begin(), ms.end(), 0);
    std::cout << sum << "\n";
    
    // filter out and erase the values greater than 30
    std::cout << ms.size() << "\n";
    for (auto i = ms.begin(), last = ms.end(); i != last; ) {
        if ((*i) > 30) {
            i = ms.erase(i);
        }
        else {
            ++i;
        }
    }
    std::cout << ms.size() << "\n";

    // use async produce some random numbers
    auto fut2 = std::async(std::launch::async, []() {
        for (int i = 0; i < 10; i++) {
            cout << my_rand() % 10 << "\n";
        }
        cout << "\n";
        int b = 0;
        while (1) {
            try {
                b = my_rand();
                if (b % 2 == 0) {
                    continue;
                }
                else {
                    throw std::runtime_error("Something Bad happened here");
                }
            }
            catch (std::exception const& e)
            {
                std::cout << "Exception: " << e.what() << "\n";
                exit(1);
            }
        }
        return b;
    });
    cout << fut2.get();
    
}
