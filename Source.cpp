#include <iostream>
#include <cstdlib> 
#include <ctime> 
#include <thread>
#include <vector>
#include <mutex>
#include <future>
using namespace std;

// g++ source.cpp -g -Wall -std=c++2a

double get_random(int rand) {
    return (double)(rand % 100 + 1);
}

int main(int argc, char* argv[])
{
    std::srand(666);

    std::vector<std::future<double>> futures;
    for (int i = 0; i < 20; i++) {
        auto fut = std::async(std::launch::async, [] {
            double total = 0;
            for (int x = 0; x < 5000000; ++x) {
                total += get_random(std::rand());
            }
            return total;
        });
        futures.push_back(std::move(fut));
    }

    double grandTotal = 0;
    for (int x = 0; x < futures.size(); x++) {
        grandTotal += futures[x].get();
    }
    cout << grandTotal << "Average of 100 million random numbers = "
        << grandTotal / 100000000.0 << "\n";

    return 0;
}
